#!/bin/bash

## script from https://gitlab.com/naturalis/core/ansible-netdc/-/blob/master/playbooks/oob-b_backup_restic.yml
## to be merged later

FILE=/opt/backup/config.yml
DATE=$(date +%Y-%m-%d)
#LAST_WEEK=$(date +%Y-%m-%d --date="1 week ago")
LAST_MONTH=$(date +%Y-%m-%d --date="1 month ago")
BACKUP_ERRORS=0

# General variables from config.yml
NET=$(yq '.gen_config.location' < $FILE) 
B_ROOT=$(yq '.gen_config.backup_dir' < $FILE)
MM_TOKEN=$(yq '.gen_config.mm_url_token' < $FILE)


## debug
#echo "$TAG" - "$NET" - "$B_ROOT" - "$REMOTE_MAAS_ROOT" - "$REMOTE" -  "$REMOTE_USER" - "$MM_TOKEN"

# shellcheck disable=SC1090
source "$MM_TOKEN"

# shellcheck source=/opt/backup/*.txt

function prep_backup_dir {
    mkdir -p "$B_ROOT"
    chown -R root:ubuntu "$B_ROOT"
    chmod 770 "$B_ROOT"
}

function fw_backup {
    B_PATH="$B_ROOT"/"$FW"
    mkdir -p "$B_PATH"
    chown root:ubuntu "$B_PATH"
    chmod 770 "$B_PATH"
    CONN_TEST=$(/usr/bin/curl --max-time 10 -I -s -k -u "$KEY":"$SECRET" https://"$FW"/api/core/backup/download/this | head -1 || echo connection_test_failed )

    if [[ "$CONN_TEST" != *"200"* ]]
    then
        echo "Result of the HTTP request is ""$CONN_TEST"" - no backup made - check connection "
        MSG="connection_test_failed"
        mm_post
        return 1
    fi

    if (/usr/bin/curl --max-time 10 --output "$B_PATH"/"$FW"-backup-"$DATE".xml -s -k -u "$KEY":"$SECRET" https://"$FW"/api/core/backup/download/this)
    then
        MSG=backup_ok
        chown root:ubuntu "$B_PATH"/"$FW"-backup-"$DATE".xml || MSG=chown_failed
        chmod 640 "$B_PATH"/"$FW"-backup-"$DATE".xml || MSG=chmod_failed
    else
        MSG=backup_fetch_failed
    fi

    if [ -f "$B_PATH"/"$FW"-backup-"$LAST_MONTH".xml ]
    then
        rm "$B_PATH"/"$FW"-backup-"$LAST_MONTH".xml || MSG=cleanupfailed
    fi

    echo "Result of backup script for $NAME: $MSG"
    SIZE=$(ls -lh "$B_PATH"/"$FW"-backup-"$DATE".xml)
    echo "$SIZE"
}

function mm_post(){
    curl -s -X POST --data-urlencode "payload={\"text\": \"$NET backup script on $NAME on $FW msg::  $MSG\"}"  "$MM_URL" > /dev/null
}

function backup_error () {
    echo "Backup of $1 exited with error code: $2"
    BACKUP_ERRORS=$(( BACKUP_ERRORS + 1 ))
}

function backup_exit () {
    if [[ $BACKUP_ERRORS -gt 0 ]]; then
        echo "In total $BACKUP_ERRORS backup errors occurred during this run."
        exit 1
    else
        echo "No errors occurred during backup of firewalls. Have a nice day!"
        exit 0
    fi
}

## prep backup root
prep_backup_dir

# loop Firewalls
for IP in $(yq .firewalls.[].address < "$FILE")
do
    NAME=$(cat $FILE | yq ".firewalls[] | select(.address == \"$IP\")|.name")
    TOKEN_FILE=$(cat $FILE | yq ".firewalls[] | select(.address == \"$IP\")|.tokenfile")
    #echo $IP $NAME $TOKEN_FILE
    FW=$IP
    # shellcheck disable=SC1090
    source "${TOKEN_FILE}"
    fw_backup || backup_error "$NAME" $?
    mm_post
done

backup_exit
