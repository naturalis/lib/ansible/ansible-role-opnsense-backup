


# role for oob server to make backups from opnense firewalls 

and post results on mattermosts
###

### Ubuntu
role is ubuntu > 20.4 and uses snap to install yq 

### hashicorp vault

hashicorp vault is used to store the crednetials for opnsense and Mattermost


### opnsense
on opnsense 
* no plugin needed anymore, it's now part of core install
* create backup user 
* give rights to backup user like
```
Effective Privileges 	
  Inherited from 	Type 	Name
	GUI 	Diagnostics: Configuration History
        - /ui/core/backup/history*
        - /api/core/backup/*
```
* create api-key 

* try curl like:

```
/usr/bin/curl --max-time 10 --output "$B_PATH"/"$FW"-backup-"$DATE".xml -s -k -u "$KEY":"$SECRET" https://"$FW"/api/core/backup/download/this
```



### matermost
on matter most <channel>/integrations/incoming_webhooks 
https://developers.mattermost.com/integrate/webhooks/incoming/
* create 


stub Yaml

```yaml
---
gen_config:
   location: net<x>
   tag: NET_<x>
   backup_dir: /data/backup
   mm_url_token: /opt/backup/mm_url_token.txt

firewalls:
- name: net-firewall-a
  address: 192.168.1.2
  tokenfile : /opt/backup/secret.txt

- name:  net-firewall-b
  address: 192.168.1.3
  tokenfile : /opt/backup/secret.txt

```

stub bash script:

```bash

#!/bin/bash

FILE=/opt/backup/config.yml
DATE=$(date +%Y-%m-%d)
LAST_WEEK=$(date +%Y-%m-%d --date="1 week ago")
LAST_MONTH=$(date +%Y-%m-%d --date="1 month ago")

# General variables from config.yml

NET=$(cat $FILE | yq '.gen_config.location')
B_ROOT=$(cat $FILE | yq '.gen_config.backup_dir')
TAG=$(cat $FILE | yq '.gen_config.tag')
MM_TOKEN=$(cat $FILE | yq '.gen_config.mm_url_token')


## debug info
echo $TAG - $NET - $B_ROOT - $MM_TOKEN


FILE=/opt/backup/config.yml

for IP in $(cat $FILE | yq .firewalls.[].address)
do

        NAME=$(cat $FILE | yq ".firewalls[]  | select(.address == \"$IP\")|.name")
	TOKEN_FILE=$(cat $FILE | yq ".firewalls[]  | select(.address == \"$IP\")|.tokenfile")
	echo $IP $NAME $TOKEN_FILE


done
```



